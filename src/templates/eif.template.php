<script type="text/javascript">
  (function(l,i,v,e,t,c,h){
  l['LiveGamesObject']=t;l[t]=l[t]||function(){(l[t].q=l[t].q||[]).push(arguments)},
  l[t].l=1*new Date();c=i.createElement(v),h=i.getElementsByTagName(v)[0];
  c.async=1;c.src=e;h.parentNode.insertBefore(c,h)
  })(window,document,'script','//embed.livegames.io/e-if.js','lg');
  if(lg){
      lg('sign', '[@token]');
      lg('currency', '[@currency]');
      lg('bgColor', '[@bgColor]');
      lg('frames', [
          [@frameList]
      ]);
  }
</script>
